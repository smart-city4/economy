﻿using Economy.Application.UseCase.Authorizes.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IMediator _mediator;
        public AuthController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Sign(LoginCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpPost("Register")]
        [Authorize(Policy = "AdminActions")]
        public async Task<IActionResult> Create(RegisterCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
    }
}
