﻿using Economy.Application.RabbitMQs;
using Economy.Application.UseCase.InDebtOperation.Command;
using Economy.Application.UseCase.InDebtOperation.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class InDebtController : ControllerBase
    {
        private readonly IMediator _mediator;
        public InDebtController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        //[Authorize(Policy = "SecurityActions")]
        public async Task<IActionResult> Create(CreateInDebtCommand command)
        {
            SecurityRabbitMQ.Publisher(command);
            return Ok();
            /*return Ok(await _mediator.Send(command));*/
        }
        [HttpPatch("Pay")]
        public async Task<IActionResult> Pay(DebtPaymentCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet("ByNumber")]
        public async Task<IActionResult> Get([FromQuery] GetDebtCommad command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet("All")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _mediator.Send(new GetAllDebtCommand()));
        }
    }
}
