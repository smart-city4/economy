﻿using Economy.Application.UseCase.Properties.Commands;
using Economy.Application.UseCase.Properties.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PropertyController : ControllerBase
    {
        private readonly IMediator _mediator;
        public PropertyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("Create/Residencial")]
        [Authorize(Policy = "AdminActions")]
        public async Task<IActionResult> CreateResidencial(CreateResidencialCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpPost("Create/NonResidencial")]
        [Authorize(Policy = "AdminActions")]
        public async Task<IActionResult> CreateResidencial(CreateNonResidencialCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpGet("ByG_Id")]
        public async Task<IActionResult> GetAddress([FromQuery] GetResidencialAddressByG_IdQuery query)
        {
            return Ok(await _mediator.Send(query));
        }

        [HttpGet("All/Address")]
        [Authorize(Policy = "GovernmentActions")]
        public async Task<IActionResult> GetAdresses()
        {
            return Ok(await _mediator.Send(new GetAllResidencialAddressCommand()));
        }

    }
}
