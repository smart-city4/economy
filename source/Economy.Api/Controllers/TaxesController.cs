﻿using Economy.Application.UseCase.Tax.Commands;
using Economy.Application.UseCase.Tax.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaxesController : ControllerBase
    {
        private readonly IMediator _mediator;
        public TaxesController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        [Authorize(Policy = "AdminActions")]
        public async Task<IActionResult> Create(CreateTaxCommand command)
        {
            return Ok(await _mediator.Send(command));
        }

        [HttpGet("All/Taxes")]
        [Authorize(Policy = "GovernmentActions")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _mediator.Send(new GetAllTaxesCommand()));
        }
    }
}
