﻿using Economy.Application.UseCase.TransactionsOperation.Command;
using Economy.Application.UseCase.TransactionsOperation.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.CompilerServices;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransactionController : ControllerBase
    {
        private readonly IMediator _mediator;
        public TransactionController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost("ByUserG_Id")]
        public async Task<IActionResult> Create(TransactionByIdCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
        [HttpPost("ByAccountNumber")]
        public async Task<IActionResult> Create(TransactionByAccountNumberCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet("InCome")]
        public async Task<IActionResult> Get([FromQuery] GetInComeTransactionByIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet("Expanditure")]
        public async Task<IActionResult> Get([FromQuery] GetExpanditureTransactionByIdCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpGet("All")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _mediator.Send(new GetAllTransactionCommand()));
        }
    }
}
