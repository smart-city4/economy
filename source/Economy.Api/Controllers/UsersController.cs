﻿using Economy.Application.UseCase.HttpClients;
using Economy.Application.UseCase.User.Command;
using Economy.Application.UseCase.User.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(Policy = "GovernmentActions")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;
        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateUserCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
        [HttpPost("Pensioner/Create")]
        public async Task<IActionResult> Create(CreatePensionerCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
        [HttpPatch("Update")]
        public async Task<IActionResult> Update(UpdatePensionerCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }
        [HttpGet("All")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _mediator.Send(new GetAllCommand());
            return Ok(result);
        }
        [HttpGet]
        [Route("{G_Id}")]
        public async Task<IActionResult> Get([FromRoute] int G_Id)
        {
            var result = await _mediator.Send(new GetUserCommand() { G_Id = G_Id });
            return Ok(result);
        }
        [HttpGet("ByAddress")]
        public async Task<IActionResult> Get([FromQuery] GetUserByAddressCommand command)
        {
            return Ok(await _mediator.Send(command));
        }
        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(DeleteUserCommand command)
        {
            var result = await _mediator.Send(command);
            return Ok(result);
        }

        [HttpGet("HttpCLient")]
        public async Task<IActionResult> GetFromGovernment()
        {
            return Ok(await _mediator.Send(new GetGovernmentDataCommand()));
        }
    }
}
