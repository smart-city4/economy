﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Abstraction
{
    public interface IGenerateAccountNumber
    {
        int AccountNumber { get; }
    }
}
