﻿using AutoMapper;
using Economy.Application.Mapper;
using Economy.Application.RabbitMQs;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Economy.Application
{
    public static class DepencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection _services)
        {
            var mappingConfig = new MapperConfiguration(x =>
            {
                x.AddProfile(new Mapping());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            _services.AddSingleton(mapper);
            _services.AddMediatR(typeof(DepencyInjection).Assembly);
            _services.AddHttpClient("datausa", config =>
            {
                config.BaseAddress = new Uri("https://datausa.io/");
            });
            _services.AddHostedService<SecurityRabbitMQ>();
            return _services;
        }
    }
}