﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Exceptions
{
    public class LoginException : Exception
    {
        private const string _message = "Login or Password error";
        public LoginException() : base(_message){ }
    }
}
