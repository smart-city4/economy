﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        private const string _message = "Not found";
        public NotFoundException(string type)
            :base($"{type} {_message}") { }
        public NotFoundException() 
            :base(_message) { }
    }
}
