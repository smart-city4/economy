﻿using AutoMapper;
using Economy.Application.Models.ViewModels;
using Economy.Application.UseCase.User.Command;
using Economy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Mapper
{
    public class Mapping : Profile
    {
        public Mapping()
        {
            CreateMap<User, UserViewModel>().ReverseMap();
            CreateMap<CreateUserCommand, User>().ForMember(d => d.G_Id, o => o.MapFrom(x => x.G_Id));
            CreateMap<CreatePensionerCommand, Pensioner>().ReverseMap();
            CreateMap<Pensioner, PensionerViewModel>().ReverseMap();
            CreateMap<User, Pensioner>().ReverseMap();
            CreateMap<InDebt, DebtViewModel>().ReverseMap();
            CreateMap<NonResidencialProperty, NonResidencialViewModel>().ReverseMap();
            CreateMap<Address, AddressViewModel>()
                .ForMember(d => d.ApartmentNumber, x => x.MapFrom(a => a.ApartmentNumber))
                .ForMember(d => d.HomeNumber, x => x.MapFrom(a => a.HomeNumber))
                .ForMember(d => d.Street, x => x.MapFrom(a => a.Street))
                .ForMember(d => d.District, x => x.MapFrom(a => a.District))
                .ForMember(d => d.Region, x => x.MapFrom(a => a.Region));
            CreateMap<Transaction, TransactionViewModel>()
                .ForMember(d => d.FromUserId, o => o.MapFrom(x => x.FromUserId))
                .ForMember(d => d.ToUserId, o => o.MapFrom(x => x.ToUserId))
                .ForMember(d => d.TransferSumm, o => o.MapFrom(x => x.Amount))
                .ForMember(d => d.Description, o => o.MapFrom(x => x.Description))
                .ForMember(d => d.FromAccountNumber, o => o.MapFrom(x => x.FromUser.AccountNumber))
                .ForMember(d => d.ToAccountNumber, o => o.MapFrom(x => x.ToUser.AccountNumber))
                .ForMember(d => d.TransferDate, o => o.MapFrom(x => x.TransactionTime));
                /*.ForMember(a => a.UserId, b => b.MapFrom(x => x.UserId))
                .ForMember(a => a.DecideNumber, b => b.MapFrom(x => x.DecideNumber))
                .ForMember(a => a.DebtSum, b => b.MapFrom(x => x.DebtSum));*/
            //CreateMap<List<User>, List<UserViewModel>>().ForAllMembers(d => d.)
        }
    }
}
