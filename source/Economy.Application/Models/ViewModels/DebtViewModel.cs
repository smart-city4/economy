﻿using Economy.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Models.ViewModels
{
    public class DebtViewModel
    {
        public int UserId { get; set; }
        public decimal DebtSum { get; set; }
        public string DecideNumber { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime DeadLine { get; set; } = DateTime.Now.AddDays(30);
        public bool IsPaid { get; set; } = false;
    }
}
