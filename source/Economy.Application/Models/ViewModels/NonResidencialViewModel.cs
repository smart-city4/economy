﻿using Economy.Domain.Entities;
using Economy.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Models.ViewModels
{
    public class NonResidencialViewModel
    {
        public int UserId { get; set; }
        public string PropertyNumber { get; set; }
        public string Name { get; set; }
        public NonResidencials Type { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
