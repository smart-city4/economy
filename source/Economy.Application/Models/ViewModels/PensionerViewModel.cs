﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Models.ViewModels
{
    public class PensionerViewModel
    {
        public int G_Id { get; set; }
        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal AllowanceSum { get; set; }
        public DateTime BePensionerDate { get; set; }
    }
}
