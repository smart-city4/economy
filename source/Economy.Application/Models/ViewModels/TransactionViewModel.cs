﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.Models.ViewModels
{
    public class TransactionViewModel
    {
        public int FromUserId { get; set; }
        public int FromAccountNumber { get; set; }
        public int ToUserId { get; set; }
        public int ToAccountNumber { get; set; }
        public decimal TransferSumm { get; set; }
        public string Description { get; set; }
        public DateTime TransferDate { get; set; }
    }
}
