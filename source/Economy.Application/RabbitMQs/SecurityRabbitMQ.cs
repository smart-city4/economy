﻿using Economy.Application.UseCase.InDebtOperation.Command;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Channels;

namespace Economy.Application.RabbitMQs
{
    public class SecurityRabbitMQ : BackgroundService
    {
        private readonly IServiceProvider _provider;
        private readonly IModel _channel;
        public SecurityRabbitMQ(IServiceProvider provider)
        {
            _provider = provider;

            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };
            var connection = factory.CreateConnection();

            _channel = connection.CreateModel();

            /*_channel.QueueDeclare(queue: "security",
                durable: false, exclusive: false,
                autoDelete: false, arguments: null);*/

            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
        }
        private async void ReceiveHandle(object sender, BasicDeliverEventArgs model)
        {
            var message = Encoding.UTF8.GetString(model.Body.ToArray());
            CreateInDebtCommand command = JsonSerializer.Deserialize<CreateInDebtCommand>(message);

            using var scope = _provider.CreateScope();
            IMediator _service = scope.ServiceProvider.GetService<IMediator>();
            var result = await _service.Send(command);
            Console.WriteLine(result);
            _channel.BasicAck(deliveryTag: model.DeliveryTag, multiple: false);

        }
        public static async void Publisher(CreateInDebtCommand command)
        {
            var message = JsonSerializer.Serialize(command);
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: "security",
                       durable: true,
                       autoDelete: true,
                       exclusive: false,
                       arguments: null);
            var body = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "", routingKey: "security", mandatory: false, basicProperties: null, body: body);

        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {

            var consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queue: "security",
                autoAck: false,
                consumer: consumer);
            var periodic = new PeriodicTimer(TimeSpan.FromSeconds(15));
            while (await periodic.WaitForNextTickAsync(stoppingToken))
            {
                consumer.Received += ReceiveHandle;
            }
            /*consumer.Received += ReceiveHandle;*/
        }
    }
}