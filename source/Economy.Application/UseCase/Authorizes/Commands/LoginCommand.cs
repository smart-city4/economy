﻿using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Authorizes.Commands
{
    public class LoginCommand : ICommand<string>
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
    public class LoginCommandHandler : ICommandHandler<LoginCommand, string>
    {
        private readonly IAppDbContext _context;
        private readonly IHashService _hashService;
        private readonly ITokenService _tokenService;

        public LoginCommandHandler(IAppDbContext context, IHashService hashService, ITokenService tokenService)
        {
            _context = context;
            _hashService = hashService;
            _tokenService = tokenService;
        }
        public async Task<string> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var loginUser = await _context.LoginForAccesses.FirstOrDefaultAsync(x => x.UserName== request.UserName, cancellationToken);
            if (loginUser == null)
            {
                throw new NotFoundException();
            }
            if (loginUser.Password != _hashService.GetHash(request.Password))
            {
                throw new LoginException();
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, loginUser.Id.ToString()),
                new Claim(ClaimTypes.Name, loginUser.UserName),
                new Claim(ClaimTypes.Email, loginUser.Email)
            };

            if (loginUser.UserRole == Domain.Enums.Role.Admin)
            {
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Admin)));
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Transport)));
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Government)));
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Security)));
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Hospital)));
            }

            else if(loginUser.UserRole == Domain.Enums.Role.Government)
            {
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Government)));
            }

            else if (loginUser.UserRole == Domain.Enums.Role.Hospital)
            {
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Hospital)));
            }

            else if (loginUser.UserRole == Domain.Enums.Role.Security)
            {
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Security)));
            }

            else if (loginUser.UserRole == Domain.Enums.Role.Transport)
            {
                claims.Add(new Claim(ClaimTypes.Role, nameof(Domain.Enums.Role.Transport)));
            }

            return _tokenService.GetAccessToken(claims.ToArray());
        }
    }
}
