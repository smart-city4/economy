﻿using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Domain.Entities;
using Economy.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Authorizes.Commands
{
    public class RegisterCommand : ICommand<bool>
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public Role UserRole { get; set; }
    }
    public class RegisterCommandHandler : ICommandHandler<RegisterCommand, bool>
    {
        private readonly IAppDbContext _context;
        private readonly IHashService _hashService;
        public RegisterCommandHandler(IAppDbContext context, IHashService hashService)
        {
            _context = context;
            _hashService = hashService;
        }
        public async Task<bool> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var username = await _context.LoginForAccesses.FirstOrDefaultAsync(x => x.UserName == request.UserName, cancellationToken);
            if (username != null)
            {
                throw new AlreadyExistsException("UserName");
            }
            var email = await _context.LoginForAccesses.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);
            if (email != null)
            {
                throw new AlreadyExistsException("Email");
            }

            var passwordHash = _hashService.GetHash(request.Password);
            LoginForAccess Login = new LoginForAccess()
            {
                UserName = request.UserName,
                Email = request.Email,
                Password = passwordHash,
                UserRole = request.UserRole
            };

            await _context.LoginForAccesses.AddAsync(Login, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
