﻿using Economy.Application.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.HttpClients
{
    public class GetGovernmentDataCommand : ICommand<string>
    {
    }
    public class GetGovernmentDataCommandHandler : ICommandHandler<GetGovernmentDataCommand, string>
    {
        private readonly HttpClient _httpClient;

        public GetGovernmentDataCommandHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("datausa");
        }

        public async Task<string> Handle(GetGovernmentDataCommand request, CancellationToken cancellationToken)
        {
            var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJhZG1pbkBnbWFpbC5jb20iLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOlsiQWRtaW4iLCJUcmFuc3BvcnQiLCJHb3Zlcm5tZW50IiwiU2VjdXJpdHkiLCJIb3NwaXRhbCJdLCJqdGkiOiJhMmI0NjVjMi1mNjVkLTQ2ZDQtOTQ2MS01YzU3YmM0ZWYzYzUiLCJpYXQiOiIxNS8wMy8yMDIzIDA2OjIyOjQ1IiwiZXhwIjoxNjc4OTQ3NzY1LCJpc3MiOiIxMjcuMC4wLjEiLCJhdWQiOiJFY29ub215In0.PjdT39hs4Bme2IUulWPLYMusrjUI9UD0mQekCuBmxI8";
            var debturl = "https://localhost:44311/api/InDebt/All";

            /*var user = new
            {
                userName = "admin",
                password = "12345"
            };*/

            //using var content = new StringContent(JsonSerializer.Serialize(user));
            //content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            //var response = await _httpClient.PostAsync("https://localhost:44311/api/Auth/Login", content);
            var req = new HttpRequestMessage(HttpMethod.Get, debturl);

            req.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);



            var response = await _httpClient.SendAsync(req);
            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }
    }
}
