﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.InDebtOperation.Command
{
    public class CreateInDebtCommand : ICommand<DebtViewModel>
    {
        [Required]
        public int UserG_Id { get; set; }
        [Required]
        public decimal DebtSum { get; set; }
        [Required]
        public string DecideNumber { get; set; }
        public string? Description { get; set; }
    }

    public class CreateInDebtCommandHandler : ICommandHandler<CreateInDebtCommand, DebtViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public CreateInDebtCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<DebtViewModel> Handle(CreateInDebtCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id== request.UserG_Id, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }
            InDebt debt = new InDebt();
            debt.UserId = user.Id;
            debt.DecideNumber = request.DecideNumber;
            debt.DebtSum = request.DebtSum;
            debt.Description = request.Description;
            debt.DeadLine = debt.CreatedDate.AddDays(30);
            await _context.Debts.AddAsync(debt, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return _mapper.Map<DebtViewModel>(debt);
        }
    }
}
