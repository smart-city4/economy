﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Application.UseCase.TransactionsOperation.Command;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.InDebtOperation.Command
{
    public class DebtPaymentCommand : ICommand<DebtViewModel>
    {
        [Required]
        public int AcconutNumber { get; set; }
        [Required]
        public string DecideNumber { get; set; }
        [Required]
        public decimal DebtSum { get; set; }
    }

    public class DebtPaymentDebtCommandHandler : ICommandHandler<DebtPaymentCommand, DebtViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;
        public DebtPaymentDebtCommandHandler(IAppDbContext context, IMapper mapper, IMediator mediator)
        {
            _context = context;
            _mapper = mapper;
            _mediator = mediator;
        }
        public async Task<DebtViewModel> Handle(DebtPaymentCommand request, CancellationToken cancellationToken)
        {
            var debt = await _context.Debts.Where(p => !p.IsPaid).FirstOrDefaultAsync(x => x.DecideNumber== request.DecideNumber, cancellationToken);
            if (debt == null)
            {
                throw new NotFoundException(nameof(debt));
            }

            var user = await _context.Users.Where(p => !p.IsDeleted).FirstOrDefaultAsync(x => x.AccountNumber == request.AcconutNumber, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }

            if (user.Balance < request.DebtSum)
            {
                throw new InsufficientBalanceException();
            }
            if (request.DebtSum >= debt.DebtSum)
            {
                debt.IsPaid = true;
                user.Balance -= debt.DebtSum;
                debt.DebtSum = 0;
            }
            else
            {
                user.Balance -= request.DebtSum;
                debt.DebtSum-= request.DebtSum;
            }
            await _mediator.Send(new TransactionByAccountNumberCommand()
            {
                FromUserAccountNumber = user.AccountNumber,
                ToUserAccountNUmber = 10000004,
                TotalSum = request.DebtSum,
                Description = $"pay for {request.DecideNumber} decide"
            }, cancellationToken);
            
           /* await _mediator.Publish()*/
            
            // davlat gaznasiga tushiriladi
            await _context.SaveChangesAsync(cancellationToken);
            return _mapper.Map<DebtViewModel>(debt);
        }
    }
}
