﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.InDebtOperation.Queries
{
    public class GetAllDebtCommand : ICommand<List<DebtViewModel>>
    {
    }

    public class GetAllDebtCommandHandler : ICommandHandler<GetAllDebtCommand, List<DebtViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetAllDebtCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<DebtViewModel>> Handle(GetAllDebtCommand request, CancellationToken cancellationToken)
        {
            List<DebtViewModel> result = new List<DebtViewModel>();
            var debts = _context.Debts.ToList();
            foreach (var debt in debts)
            {
                result.Add(_mapper.Map<DebtViewModel>(debt));
            }
            return result;
        }
    }
}
