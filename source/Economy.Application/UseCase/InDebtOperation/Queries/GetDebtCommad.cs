﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.InDebtOperation.Queries
{
    public class GetDebtCommad : ICommand<DebtViewModel>
    {
        [Required]
        public string DecideNumber { get; set; }
    }

    public class GetDebtCommandHandler : ICommandHandler<GetDebtCommad, DebtViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetDebtCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<DebtViewModel> Handle(GetDebtCommad request, CancellationToken cancellationToken)
        {
            var debt = await _context.Debts.FirstOrDefaultAsync(x => x.DecideNumber== request.DecideNumber, cancellationToken);

            if (debt == null)
            {
                throw new NotFoundException("Debt");
            }
            return _mapper.Map<DebtViewModel>(debt);
        }
    }
}
