﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using Economy.Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Economy.Application.UseCase.Properties.Commands
{
    public class CreateNonResidencialCommand : ICommand<NonResidencialViewModel>
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string PropertyNumber { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public NonResidencials Type { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
    }

    public class CreateNonResidencialPropertyCommandHandler : ICommandHandler<CreateNonResidencialCommand, NonResidencialViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;
        public CreateNonResidencialPropertyCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<NonResidencialViewModel> Handle(CreateNonResidencialCommand request, CancellationToken cancellationToken)
        {
            var nrp = await _context.NonResidencialProperties.FirstOrDefaultAsync(x => x.PropertyNumber == request.PropertyNumber, cancellationToken);
            if (nrp != null)
            {
                throw new AlreadyExistsException("Non Residencial Property");
            }
            NonResidencialProperty nonResidencial = new NonResidencialProperty()
            {
                PropertyNumber = request.PropertyNumber,
                CreatedDate = DateTime.UtcNow,
                Description = request.Description,
                Price = request.Price,
                Name = request.Name,
                UserId = request.UserId,
                Type = request.Type,
            };

            await _context.NonResidencialProperties.AddAsync(nonResidencial, cancellationToken);

            var tax = await _context.Taxes.FirstOrDefaultAsync(x => x.Type == TypeOfTax.Property_Tax, cancellationToken);

            if (tax == null)
            {
                throw new NotFoundException("Tax");
            }

            UserTax userTax = new UserTax()
            {
                UserId = request.UserId,
                TaxId = tax.Id,
                Description = "Mol-Mulk Soligi",
                Amount = request.Price * Convert.ToDecimal(tax.Percent) / 100
            };

            await _context.UserTaxes.AddAsync(userTax, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return _mapper.Map<NonResidencialViewModel>(nonResidencial);
        }
    }
}
