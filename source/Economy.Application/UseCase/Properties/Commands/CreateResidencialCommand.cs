﻿using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Domain.Entities;
using Economy.Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Properties.Commands
{
    public class CreateResidencialCommand : ICommand<Unit>
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public string PropertyNumber { get; set; }
        [Required]
        public string Name { get; set; }
        public double Size { get; set; }
        [Required]
        public string Region { get; set; }
        [Required]
        public string District { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public int HomeNumber { get; set; }
        [Required]
        public int ApartmentNumber { get; set; }
        public decimal Price { get; set; }
    }

    public class CreateResidencialCommnandHandler : ICommandHandler<CreateResidencialCommand, Unit>
    {
        private readonly IAppDbContext _context;
        public CreateResidencialCommnandHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(CreateResidencialCommand request, CancellationToken cancellationToken)
        {
            var address = await _context.Addresses.Where(a => a.Region == request.Region & a.District == request.District & a.Street == request.Street & a.HomeNumber == request.HomeNumber).FirstOrDefaultAsync(x => x.ApartmentNumber == request.ApartmentNumber);
            if (address != null)
            {
                throw new AlreadyExistsException("This Address");
            }
            Address newAddress = new Address()
            {
                Region= request.Region,
                District=request.District,
                Street=request.Street,
                HomeNumber=request.HomeNumber,
                ApartmentNumber = request.ApartmentNumber
            };

            await _context.Addresses.AddAsync(newAddress, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            ResidencialProperty usertax = new ResidencialProperty()
            {
                UserId= request.UserId,
                Name = request.Name,
                PropertyNumber = request.PropertyNumber,
                Size = request.Size,
                Address = newAddress,
                Price = request.Price,
            };
            await _context.ResidencialProperties.AddAsync(usertax, cancellationToken);

            var tax = await _context.Taxes.FirstOrDefaultAsync(x => x.Type == TypeOfTax.Property_Tax, cancellationToken);

            if (tax == null)
            {
                throw new NotFoundException("Tax");
            }

            UserTax userTax = new UserTax()
            {
                UserId = request.UserId,
                TaxId = tax.Id,
                Description = "Mol-Mulk Soligi",
                Amount = request.Price * Convert.ToDecimal(tax.Percent)/100
            };

            await _context.UserTaxes.AddAsync(userTax, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
