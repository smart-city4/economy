﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Properties.Queries
{
    public class GetAllResidencialAddressCommand : ICommand<List<AddressViewModel>>
    {
    }

    public class GetAllResidencialAddressCommandHandler : ICommandHandler<GetAllResidencialAddressCommand, List<AddressViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;
        public GetAllResidencialAddressCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<AddressViewModel>> Handle(GetAllResidencialAddressCommand request, CancellationToken cancellationToken)
        {
            var addresses = await _context.Addresses.ToListAsync(cancellationToken);
            return _mapper.Map<List<AddressViewModel>>(addresses);
        }
    }
}
