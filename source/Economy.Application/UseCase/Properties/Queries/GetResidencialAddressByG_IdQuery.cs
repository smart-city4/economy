﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Properties.Queries
{
    public class GetResidencialAddressByG_IdQuery : ICommand<List<AddressViewModel>>
    {
        public int G_Id { get; set; }
    }
    public class GetResidencialAddressByG_IdHandler : ICommandHandler<GetResidencialAddressByG_IdQuery, List<AddressViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;
        public GetResidencialAddressByG_IdHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        async Task<List<AddressViewModel>> IRequestHandler<GetResidencialAddressByG_IdQuery, List<AddressViewModel>>.Handle(GetResidencialAddressByG_IdQuery request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.G_Id == request.G_Id, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException("User");
            }
            var adresses = _context.ResidencialProperties.Where(x => x.UserId== user.Id).Include(x => x.Address).Select(x => x.Address);
            /*if (residencialProperty == null)
            {
                throw new NotFoundException();
            }
            var address = await _context.Addresses.Where(x => x.Id == residencialProperty.AddressId).ToListAsync(cancellationToken);*/
            return _mapper.Map<List<AddressViewModel>>(adresses);
        }
    }
}
