﻿using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Tax.Commands
{
    public class CreateTaxCommand : ICommand<Unit>
    {
        public string Name { get; set; }
        public TypeOfTax Type { get; set; }
        public double Percent { get; set; }
    }
    public class CreateTaxCommandHandler : ICommandHandler<CreateTaxCommand, Unit>
    {
        private readonly IAppDbContext _context;
        public CreateTaxCommandHandler(IAppDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(CreateTaxCommand request, CancellationToken cancellationToken)
        {
            var tax = await _context.Taxes.FirstOrDefaultAsync(x => x.Name == request.Name, cancellationToken);
            if (tax != null)
            {
                throw new AlreadyExistsException();
            }
            Domain.Entities.Tax newTax = new Domain.Entities.Tax()
            {
                Name= request.Name,
                Type = request.Type,
                Percent = request.Percent,
            };

            await _context.Taxes.AddAsync(newTax, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
