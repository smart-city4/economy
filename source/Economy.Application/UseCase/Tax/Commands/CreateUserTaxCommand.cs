﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.UseCase.Tax.Commands
{
    public class CreateUserTaxCommand
    {
        public int UserId { get; set; }
        public string Description { get; set; }
        public int TaxId { get; set; }
        public decimal Amount { get; set; }
    }
}
