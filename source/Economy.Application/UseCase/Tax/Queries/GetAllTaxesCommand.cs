﻿using Economy.Application.Abstraction;
using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.Tax.Queries
{
    public class GetAllTaxesCommand : ICommand<List<UserTax>>
    {
    }

    public class GetAllTaxesCommandHandler : ICommandHandler<GetAllTaxesCommand, List<UserTax>>
    {
        private readonly IAppDbContext _context;
        public GetAllTaxesCommandHandler(IAppDbContext context)
        {
            _context = context;
        }
        public async Task<List<UserTax>> Handle(GetAllTaxesCommand request, CancellationToken cancellationToken)
        {
            var userTaxes = await _context.UserTaxes.ToListAsync(cancellationToken);
            return userTaxes;
        }
    }
}
