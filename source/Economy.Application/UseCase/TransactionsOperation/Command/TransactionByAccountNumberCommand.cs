﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.TransactionsOperation.Command
{
    public class TransactionByAccountNumberCommand :ICommand<TransactionViewModel>
    {
        [Required]
        public int FromUserAccountNumber { get; set; }
        [Required]
        public int ToUserAccountNUmber { get; set; }
        [Required]
        public decimal TotalSum { get; set; }
        public string? Description { get; set; }
    }
    public class TransactionByAccountNumberCommandHandler : ICommandHandler<TransactionByAccountNumberCommand, TransactionViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public TransactionByAccountNumberCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<TransactionViewModel> Handle(TransactionByAccountNumberCommand request, CancellationToken cancellationToken)
        {
            Transaction transaction = new Transaction();
            var fromUser = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.AccountNumber == request.FromUserAccountNumber, cancellationToken);
            if (fromUser == null)
            {
                throw new NotFoundException("User");
            }
            if (fromUser.Balance < request.TotalSum)
            {
                throw new InsufficientBalanceException();
            }
            var toUser = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.AccountNumber == request.ToUserAccountNUmber, cancellationToken);
            if (toUser == null)
            {
                throw new NotFoundException("User");
            }

            fromUser.Balance -= request.TotalSum;
            toUser.Balance += request.TotalSum;
            transaction.FromUserId = fromUser.Id;
            transaction.ToUserId = toUser.Id;
            transaction.Amount = request.TotalSum;
            transaction.Description = request.Description ?? "Something";

            await _context.Transactions.AddAsync(transaction, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return _mapper.Map<TransactionViewModel>(transaction);

        }
    }
}
