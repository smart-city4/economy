﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.TransactionsOperation.Command
{
    public class TransactionByIdCommand : ICommand<TransactionViewModel>
    {
        [Required]
        public int FromUserG_Id { get; set; }
        [Required]
        public int ToUserG_Id { get; set; }
        [Required]
        public decimal TotalSum { get; set; }
        public string? Description { get; set; }
    }

    public class NewTransactionCommandHandler : ICommandHandler<TransactionByIdCommand, TransactionViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public NewTransactionCommandHandler(IAppDbContext context, IMapper mapper) 
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<TransactionViewModel> Handle(TransactionByIdCommand request, CancellationToken cancellationToken)
        {
            Transaction transaction = new Transaction();
            var fromUser = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id== request.FromUserG_Id, cancellationToken);
            if (fromUser==null)
            {
                throw new NotFoundException("User");
            }
            if (fromUser.Balance < request.TotalSum)
            {
                throw new InsufficientBalanceException();
            }
            var toUser = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id == request.ToUserG_Id, cancellationToken);
            if (toUser==null)
            {
                throw new NotFoundException("User");
            }

            fromUser.Balance -= request.TotalSum;
            toUser.Balance += request.TotalSum;
            transaction.FromUserId = fromUser.Id;
            transaction.ToUserId = toUser.Id;
            transaction.Amount = request.TotalSum;
            transaction.Description = request.Description ?? "Something";

            await _context.Transactions.AddAsync(transaction, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return _mapper.Map<TransactionViewModel>(transaction);

        }
    }
}
