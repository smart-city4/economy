﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.TransactionsOperation.Queries
{
    public class GetAllTransactionCommand : ICommand<List<TransactionViewModel>>
    {
    }

    public class GetAllTransactionCommandHandler : ICommandHandler<GetAllTransactionCommand, List<TransactionViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetAllTransactionCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<TransactionViewModel>> Handle(GetAllTransactionCommand request, CancellationToken cancellationToken)
        {
            List<TransactionViewModel> transactions =await ( from transaction in _context.Transactions
                                                      select _mapper.Map<TransactionViewModel>(transaction)).ToListAsync();
            
            return transactions;
        }
    }
}
