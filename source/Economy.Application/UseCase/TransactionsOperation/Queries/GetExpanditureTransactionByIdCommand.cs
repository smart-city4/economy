﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.UseCase.TransactionsOperation.Queries
{
    public class GetExpanditureTransactionByIdCommand : ICommand<List<TransactionViewModel>>
    {
        public int Id { get; set; }
    }

    public class GetExpanditureTransactionBySHIRCommandHandler : ICommandHandler<GetExpanditureTransactionByIdCommand, List<TransactionViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetExpanditureTransactionBySHIRCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<TransactionViewModel>> Handle(GetExpanditureTransactionByIdCommand request, CancellationToken cancellationToken)
        {
            List<TransactionViewModel> transactions = await (from transaction in _context.Transactions
                                                             where transaction.FromUserId == request.Id
                                                             select _mapper.Map<TransactionViewModel>(transaction)).ToListAsync(cancellationToken);

            return transactions;
        }
    }
}
