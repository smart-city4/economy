﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.TransactionsOperation.Queries
{
    public class GetInComeTransactionByIdCommand : ICommand<List<TransactionViewModel>>
    {
        public int Id { get; set; }
    }

    public class GetTransactionBySHIRCommandHandler : ICommandHandler<GetInComeTransactionByIdCommand, List<TransactionViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetTransactionBySHIRCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<TransactionViewModel>> Handle(GetInComeTransactionByIdCommand request, CancellationToken cancellationToken)
        {
            List<TransactionViewModel> transactions = await (from transaction in _context.Transactions
                                                             where transaction.ToUserId == request.Id
                                                             select _mapper.Map<TransactionViewModel>(transaction)).ToListAsync(cancellationToken);

            return transactions;
        }
    }
}
