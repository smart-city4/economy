﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.UseCase.User.Command
{
    public class CreatePensionerCommand : ICommand<PensionerViewModel>
    {
        [Required]
        public int G_Id { get; set; }
        [Required]
        public decimal AllowanceSum { get; set; }
    }

    public class CreatePensionerCommandHandler : ICommandHandler<CreatePensionerCommand, PensionerViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public CreatePensionerCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PensionerViewModel> Handle(CreatePensionerCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id == request.G_Id, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }
            Pensioner pensioner = new Pensioner();
            pensioner = _mapper.Map<Pensioner>(user);
            pensioner.AllowanceSum = request.AllowanceSum;
            _context.Users.Remove(user);
            await _context.Pensioners.AddAsync(pensioner);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<PensionerViewModel>(pensioner);
        }
    }
}
