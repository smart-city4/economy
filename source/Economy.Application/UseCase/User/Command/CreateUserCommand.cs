﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions;
using Economy.Application.Abstraction;
using Economy.Domain.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Economy.Application.Models.ViewModels;
using Economy.Application.Exceptions;

namespace Economy.Application.UseCase.User.Command
{
    public class CreateUserCommand : ICommand<UserViewModel>
    {
        [Required]
        public int G_Id { get; set; }
    }
    public class CreateUserCommandHandler : ICommandHandler<CreateUserCommand, UserViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IGenerateAccountNumber _generateAccountNumber;

        public CreateUserCommandHandler(IAppDbContext context, IMapper mapper, IGenerateAccountNumber generateAccountNumber)
        {
            _context = context;
            _mapper = mapper;
            _generateAccountNumber = generateAccountNumber;
        }

        public async Task<UserViewModel> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            Domain.Entities.User user = await _context.Users.FirstOrDefaultAsync(x => x.G_Id == request.G_Id, cancellationToken);
            if (user != null)
            {
                throw new AlreadyExistsException(nameof(user));
            }
            user = new Domain.Entities.User();
            user = _mapper.Map<Domain.Entities.User>(request);
            user.AccountNumber = _generateAccountNumber.AccountNumber;
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<UserViewModel>(user);
        }
    }
}
