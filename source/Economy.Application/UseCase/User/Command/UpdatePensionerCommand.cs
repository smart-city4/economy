﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.User.Command
{
    public class UpdatePensionerCommand : ICommand<PensionerViewModel>
    {
        [Required]
        public int G_Id { get; set; }
        [Required]
        public decimal AllowanceSum { get; set; }
    }

    public class UpdatePensionerCommandHandler : ICommandHandler<UpdatePensionerCommand, PensionerViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public UpdatePensionerCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PensionerViewModel> Handle(UpdatePensionerCommand request, CancellationToken cancellationToken)
        {
            var pensioner = await  _context.Pensioners.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id == request.G_Id, cancellationToken);
            if (pensioner == null)
            {
                throw new NotFoundException(nameof(pensioner));
            }
            pensioner.AllowanceSum = request.AllowanceSum;
            await _context.SaveChangesAsync(cancellationToken);

            return _mapper.Map<PensionerViewModel>(pensioner);
        }
    }
}
