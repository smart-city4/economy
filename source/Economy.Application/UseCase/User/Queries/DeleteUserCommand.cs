﻿using Economy.Application.Abstraction;
using Economy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Economy.Application.Exceptions;

namespace Economy.Application.UseCase.User.Queries
{
    public class DeleteUserCommand : ICommand<bool>
    {
        [Required]
        public int G_Id { get; set; }
    }
    public class DeleteUserCommandHandler : ICommandHandler<DeleteUserCommand, bool>
    {
        private readonly IAppDbContext _context;

        public DeleteUserCommandHandler(IAppDbContext context)
        {
            _context = context;
        }
        public async Task<bool> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id == request.G_Id, cancellationToken);
            if (user == null)
            {
                throw new NotFoundException(nameof(user));
            }
            user.IsDeleted = true;
            user.DeletedDate = DateTime.UtcNow;
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}
