﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Models.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Application.UseCase.User.Queries
{
    public class GetAllCommand : ICommand<List<UserViewModel>>
    {
    }

    public class GetAllCommandHandler : ICommandHandler<GetAllCommand, List<UserViewModel>>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetAllCommandHandler(IAppDbContext context, IMapper mapper) 
        {
            _context = context;
            _mapper = mapper;
        }
        public Task<List<UserViewModel>> Handle(GetAllCommand request, CancellationToken cancellationToken)
        {
            List<Domain.Entities.User> users = _context.Users.Where(x => !x.IsDeleted).ToList();
            List<UserViewModel> result = new List<UserViewModel>();
            foreach(var user in users)
            {
                result.Add(_mapper.Map<UserViewModel>(user));
            }

            return Task.FromResult(result);
        }
    }
}
