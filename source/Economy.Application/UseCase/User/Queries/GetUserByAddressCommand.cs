﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.User.Queries
{
    public class GetUserByAddressCommand : ICommand<UserViewModel>
    {
        [Required]
        public string Region { get; set; }
        [Required]
        public string District { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public int HomeNumber { get; set; }
        public int? ApartmentNumber { get; set; }
    }
    public class GetUserByAddressCommandHandler : ICommandHandler<GetUserByAddressCommand, UserViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;


        public GetUserByAddressCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserViewModel> Handle(GetUserByAddressCommand request, CancellationToken cancellationToken)
        {
            var address = await _context.Addresses.Include(x => x.ResidencialProperty).FirstOrDefaultAsync(x => x.Region == request.Region & x.District == request.District & x.Street == request.Street & x.HomeNumber == request.HomeNumber & x.ApartmentNumber == request.ApartmentNumber, cancellationToken);
            if (address == null)
            {
                throw new NotFoundException("Address");
            }

            var founUser = from user in _context.Users
                       where user.Id == address.ResidencialProperty.UserId
                       select user;

            return _mapper.Map<UserViewModel>(await founUser.FirstOrDefaultAsync(cancellationToken));
        }
    }
}
