﻿using AutoMapper;
using Economy.Application.Abstraction;
using Economy.Application.Exceptions;
using Economy.Application.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Economy.Application.UseCase.User.Queries
{
    public class GetUserCommand : ICommand<UserViewModel>
    {
        [Required]
        public int G_Id { get; set; }
    }

    public class GetUserCommandHandler : ICommandHandler<GetUserCommand, UserViewModel>
    {
        private readonly IAppDbContext _context;
        private readonly IMapper _mapper;

        public GetUserCommandHandler(IAppDbContext context, IMapper mapper)
        {
            _context = context; _mapper = mapper;
        }
        public async Task<UserViewModel> Handle(GetUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.Where(x => !x.IsDeleted).FirstOrDefaultAsync(x => x.G_Id == request.G_Id);
            
            if (user == null)
            {
                throw new NotFoundException(nameof(User));
            }

            return _mapper.Map<UserViewModel>(user);
        }
    }
}
