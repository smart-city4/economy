﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class Address
    {
        public int Id { get; set; }
        public string Region { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public int HomeNumber { get; set; }
        public int? ApartmentNumber { get; set; }
        public ResidencialProperty ResidencialProperty { get; set; }
    }
}
