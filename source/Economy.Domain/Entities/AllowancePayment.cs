﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class AllowancePayment
    {
        public int Id { get; set; }
        public int PensionerId { get; set; }
        public Pensioner Pensioner { get; set; }
        public DateTime PaymentDate { get; set; } = DateTime.UtcNow;
    }
}
