﻿using Economy.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class NonResidencialProperty
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public string PropertyNumber { get; set; }
        public string Name { get; set; }
        public NonResidencials Type { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedDate { get; set; }
    }
}
