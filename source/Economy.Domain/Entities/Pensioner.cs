﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class Pensioner : User
    {
        public decimal AllowanceSum { get; set; }
        public DateTime BePensionerDate { get; set; } = DateTime.UtcNow;

        public ICollection<AllowancePayment> AllowancePayments { get; set; } = new HashSet<AllowancePayment>();
    }
}
