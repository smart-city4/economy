﻿using Economy.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class Tax
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TypeOfTax Type { get; set; }
        public double Percent { get; set; }
    }
}
