﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class TaxPayment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public decimal TotalSum { get; set; }
        public DateTime PaymentDate { get; set; } = DateTime.UtcNow;

        public ICollection<UserTax> UserTaxes { get; set; } = new HashSet<UserTax>();
    }
}
