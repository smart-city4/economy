﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public int FromUserId { get; set; }
        public User FromUser { get; set; }
        public int ToUserId { get; set; }
        public User ToUser { get; set; }
        public string Description { get; set; } = "No Description";
        public decimal Amount { get; set; }
        public DateTime TransactionTime { get; set; } = DateTime.UtcNow;
    }
}
