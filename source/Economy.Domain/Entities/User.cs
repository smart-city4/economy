﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class User
    {
        public int Id { get; set; }
        public int G_Id { get; set; }
        public int AccountNumber { get; set; }
        public decimal Balance { get; set; } = 0;
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedDate { get; set; }

        public ICollection<TaxPayment> TaxPayments { get; set; } = new HashSet<TaxPayment>();
        public ICollection<NonResidencialProperty> NonResidencialProperties { get;set; } = new HashSet<NonResidencialProperty>();
        public ICollection<ResidencialProperty> ResidencialProperties { get; set; } = new HashSet<ResidencialProperty>();
        public ICollection<UserTax> UserTaxes { get; set; } = new HashSet<UserTax>();
        public ICollection<Transaction> Profits { get; set; } = new HashSet<Transaction>();
        public ICollection<Transaction> Costs { get; set; } = new HashSet<Transaction>();
        public ICollection<InDebt> Debts { get; set; } = new HashSet<InDebt>();
    }
}
