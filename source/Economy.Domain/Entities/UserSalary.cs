﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    public class UserSalary
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public decimal Salary { get; set; }
        public string Position { get; set; }
        public DateTime PaymentDate { get; set; } = DateTime.UtcNow;
    }
}
