﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Enums
{
    public enum Role
    {
        Admin = 1, Government = 2, Hospital = 3, Security = 4, Transport = 5
    }
}
