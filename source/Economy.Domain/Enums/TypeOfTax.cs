﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Enums
{
    public enum TypeOfTax
    {
        Property_Tax = 1, Profit_Tax = 2, Land_Tax = 3
    }
}
