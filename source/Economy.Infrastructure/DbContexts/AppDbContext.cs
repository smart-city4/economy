﻿using Economy.Application.Abstraction;
using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) 
            : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Tax> Taxes { get; set; }
        public DbSet<InDebt> Debts { get; set; }
        public DbSet<Pensioner> Pensioners { get; set; }
        public DbSet<TaxPayment> TaxPayments { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<UserSalary> UsersSalaries { get; set; }
        public DbSet<ResidencialProperty> ResidencialProperties { get; set; }
        public DbSet<NonResidencialProperty> NonResidencialProperties { get; set; }
        public DbSet<LoginForAccess> LoginForAccesses { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<UserTax> UserTaxes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }
        
    }
}
