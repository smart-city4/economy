﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    public class AllowancePaymentTypeConfiguration : IEntityTypeConfiguration<AllowancePayment>
    {
        public void Configure(EntityTypeBuilder<AllowancePayment> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
