﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastructure.DbContexts.EntityTypeConfigurations
{
    public class LoginForAccessTypeConfiguration : IEntityTypeConfiguration<LoginForAccess>
    {
        public void Configure(EntityTypeBuilder<LoginForAccess> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.UserName).IsUnique();
            builder.HasIndex(x => x.Email).IsUnique();
        }
    }
}
