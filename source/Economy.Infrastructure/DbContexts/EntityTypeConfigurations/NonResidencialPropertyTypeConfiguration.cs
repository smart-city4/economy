﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    internal class NonResidencialPropertyTypeConfiguration : IEntityTypeConfiguration<NonResidencialProperty>
    {
        public void Configure(EntityTypeBuilder<NonResidencialProperty> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.PropertyNumber).IsUnique();
        }
    }
}
