﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    internal class PensionerTypeConfiguration : IEntityTypeConfiguration<Pensioner>
    {
        public void Configure(EntityTypeBuilder<Pensioner> builder)
        {
            builder.HasMany(p => p.AllowancePayments).WithOne(u => u.Pensioner).HasForeignKey(k => k.PensionerId);
        }
    }
}
