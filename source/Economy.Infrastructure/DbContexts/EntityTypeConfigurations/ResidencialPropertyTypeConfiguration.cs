﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    internal class ResidencialPropertyTypeConfiguration : IEntityTypeConfiguration<ResidencialProperty>
    {
        public void Configure(EntityTypeBuilder<ResidencialProperty> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.PropertyNumber).IsUnique();
            builder.HasOne(x => x.Address).WithOne(x => x.ResidencialProperty).HasForeignKey<ResidencialProperty>(o => o.AddressId);
        }
    }
}
