﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    internal class TaxPaymentTypeConfiguration : IEntityTypeConfiguration<TaxPayment>
    {
        public void Configure(EntityTypeBuilder<TaxPayment> builder)
        {
            builder.HasKey(t => t.Id);
        }
    }
}
