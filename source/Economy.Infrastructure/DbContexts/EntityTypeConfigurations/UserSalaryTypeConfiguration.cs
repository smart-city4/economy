﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Domain.Entities
{
    internal class UserSalaryTypeConfiguration : IEntityTypeConfiguration<UserSalary>
    {
        public void Configure(EntityTypeBuilder<UserSalary> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
