﻿using Economy.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture.DbContexts.EntityTypeConfigurations
{
    internal class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(a => a.AccountNumber).IsUnique();
            builder.HasIndex(sh => sh.G_Id).IsUnique();
            builder.HasMany(t => t.UserTaxes).WithOne(u => u.User).HasForeignKey(x => x.UserId);
            builder.HasMany(d => d.Debts).WithOne(u => u.User).HasForeignKey(x => x.UserId);
            builder.HasMany(p => p.TaxPayments).WithOne(u => u.User).HasForeignKey(u => u.UserId);
            builder.HasMany(pr => pr.ResidencialProperties).WithOne(u => u.User).HasForeignKey(x =>x.UserId);
            builder.HasMany(npr => npr.NonResidencialProperties).WithOne(u =>u.User).HasForeignKey(x =>x.UserId);
            builder.HasMany(i => i.Profits).WithOne(u => u.ToUser).HasForeignKey(x => x.ToUserId);
            builder.HasMany(o => o.Costs).WithOne(u => u.FromUser).HasForeignKey(x => x.FromUserId);
        }
    }
}
