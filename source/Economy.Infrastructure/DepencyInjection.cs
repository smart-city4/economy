﻿using Economy.Application.Abstraction;
using Economy.Infrastructure.JWTService;
using Economy.Infrastructure.Service;
using Economy.Infrastucture.DbContexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastucture
{
    public static class DepencyInjection
    {
        public static IServiceCollection AddInfrastucture(this IServiceCollection _services, IConfiguration _configuration)
        {
            _services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(_configuration.GetConnectionString("DefaultConnection")));

            _services.AddScoped<IAppDbContext, AppDbContext>();
            _services.AddScoped<IGenerateAccountNumber, GenerateAccountNumber>();
            _services.AddScoped<IHashService, HashService>();
            _services.AddScoped<ITokenService, JWTService>();

            _services.Configure<JWTConfiguration>(_configuration.GetSection("JWTConfiguration"));

            _services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidAudience = _configuration["JWTConfiguration:ValidAudience"],
                        ValidIssuer = _configuration["JWTConfiguration:ValidIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWTConfiguration:Secret"]))
                    };
                });

            _services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminActions", policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role, "Admin");
                });

                options.AddPolicy("GovernmentActions", policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role, "Government");
                });
                options.AddPolicy("TransportActions", policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role, "Transport");
                });

                options.AddPolicy("SecurityActions", policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role, "Security");
                });
                options.AddPolicy("HospitalActions", policy =>
                {
                    policy.RequireClaim(ClaimTypes.Role, "Hospital");
                });
            });

            return _services;
        }
    }
}
