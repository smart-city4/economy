﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Economy.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class somechanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SHIR",
                table: "Users",
                newName: "G_Id");

            migrationBuilder.RenameIndex(
                name: "IX_Users_SHIR",
                table: "Users",
                newName: "IX_Users_G_Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "G_Id",
                table: "Users",
                newName: "SHIR");

            migrationBuilder.RenameIndex(
                name: "IX_Users_G_Id",
                table: "Users",
                newName: "IX_Users_SHIR");
        }
    }
}
