﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Economy.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddedUsertaxesTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTax_TaxPayments_TaxPaymentId",
                table: "UserTax");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTax_Taxes_TaxId",
                table: "UserTax");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTax_Users_UserId",
                table: "UserTax");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTax",
                table: "UserTax");

            migrationBuilder.RenameTable(
                name: "UserTax",
                newName: "UserTaxes");

            migrationBuilder.RenameIndex(
                name: "IX_UserTax_UserId",
                table: "UserTaxes",
                newName: "IX_UserTaxes_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTax_TaxPaymentId",
                table: "UserTaxes",
                newName: "IX_UserTaxes_TaxPaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTax_TaxId",
                table: "UserTaxes",
                newName: "IX_UserTaxes_TaxId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTaxes",
                table: "UserTaxes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTaxes_TaxPayments_TaxPaymentId",
                table: "UserTaxes",
                column: "TaxPaymentId",
                principalTable: "TaxPayments",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTaxes_Taxes_TaxId",
                table: "UserTaxes",
                column: "TaxId",
                principalTable: "Taxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTaxes_Users_UserId",
                table: "UserTaxes",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTaxes_TaxPayments_TaxPaymentId",
                table: "UserTaxes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTaxes_Taxes_TaxId",
                table: "UserTaxes");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTaxes_Users_UserId",
                table: "UserTaxes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTaxes",
                table: "UserTaxes");

            migrationBuilder.RenameTable(
                name: "UserTaxes",
                newName: "UserTax");

            migrationBuilder.RenameIndex(
                name: "IX_UserTaxes_UserId",
                table: "UserTax",
                newName: "IX_UserTax_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTaxes_TaxPaymentId",
                table: "UserTax",
                newName: "IX_UserTax_TaxPaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTaxes_TaxId",
                table: "UserTax",
                newName: "IX_UserTax_TaxId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTax",
                table: "UserTax",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTax_TaxPayments_TaxPaymentId",
                table: "UserTax",
                column: "TaxPaymentId",
                principalTable: "TaxPayments",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTax_Taxes_TaxId",
                table: "UserTax",
                column: "TaxId",
                principalTable: "Taxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTax_Users_UserId",
                table: "UserTax",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
