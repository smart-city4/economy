﻿using Economy.Application.Abstraction;
using Economy.Domain.Entities;
using Economy.Infrastucture.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economy.Infrastructure.Service
{
    public class GenerateAccountNumber : IGenerateAccountNumber
    {
        private readonly IAppDbContext _context;
        public int AccountNumber { get; set; }

        public GenerateAccountNumber(IAppDbContext context)
        {
            _context = context;
            var users = _context.Users.Select(x => x.AccountNumber);
            int number;
            if (users.Count() > 0)
            {
                number = users.Max();
            }
            else
            {
                number = 10000001;
            }
            this.AccountNumber = ++number;

        }

    }
}
